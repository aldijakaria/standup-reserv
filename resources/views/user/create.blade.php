@extends('layouts.app')

@section('content')
  <div class="container">
        <div class="row">


            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Tambah User Baru</div>
                    <div class="card-body">
                        <a href="{{ route('user.index') }}" title="Kembali"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button></a>
                        <br />
                        <br />

                        @if ($alert=Session::get('alert'))
                        <div class="alert alert-info">
                          {{ $alert }}
                        </div>
                        @endif

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ route('user.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('user.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
  </div>
@endsection
