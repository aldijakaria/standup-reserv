<div class="sidebar" data-color="black"  data-image="{{ asset('kit/img/sidebaruns.jpg') }}">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="  {{ url('/') }}" class="simple-text">
                <img src="{{ asset('logo.png') }}"  style="width: 50px;" alt="">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <ul class="nav">

              @auth('admin')
              <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('home') }}">
                      <i class="nc-icon nc-chart-pie-35"></i>
                      <p>Dashboard</p>
                  </a>
              </li>
                <li class="nav-item {{ Request::is('movie','movie/*') ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('movie.index') }}">
                      <i class="nc-icon nc-circle-09"></i>
                      <p>Movies</p>
                  </a>
              </li>
              <li class="nav-item {{ Request::is('user','user/*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('user.index').'?bulan='.date('n').'&tahun='.date('Y') }}">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>User</p>
                </a>
            </li>

          @endauth
          @auth ('web')
            @auth('admin')
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('acara') }}">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>List Acara</p>
                </a>
            </li>
            <li class="nav-item {{ Request::is('reserve','reserve/*') ? 'active' : '' }}">
              <a class="nav-link" href="{{ route('reserve.all') }}">
                  <i class="nc-icon nc-circle-09"></i>
                  <p>Pesanana</p>
              </a>
          </li>
          @endauth



        </ul>
    </div>
</div>
