<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
           <meta name="viewport"
                 content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
           <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- CSS Files -->
    <link href="{{ asset('kit/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('kit/css/light-bootstrap-dashboard.css?v=2.0.1') }}" rel="stylesheet" />


   <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}" >
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
   <!-- CSS Files -->

</head>

<body>

<center>
<b> KEMENTRIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI</b>
</center>
  <center>
  <b>  Universitas Singaperbangsa Karawang </b>
  </center>
  <center>
<b>    DAFTAR NOMINATIF TUNJANGAN</b>
  </center>
  <center>
<b>    PERIODE TUNJANGAN {{ bulan(Request::get('bulan')) }} {{ Request::get('tahun') }}</b>
  </center>

                  @yield('content')
                  <br>
                  <center>
                  <table  style="text-align:center; margin: 0 auto;">
                    <td><br>

Mengetahui <br>
Rektor
<br><br><br> <br>
H.M. WAHYUDIN ZARKASYI <br>
NIP. 19570807 1986011001

                    </td>
                    <td width="300px">

                    </td>
                    <td>
                      Karawang, {{  date('j').' '.bulan(date('n')).' '.date('Y') }} <br>
an. Kepala Biro Umum dan Keuangan <br>
       Kepala Bagian Umum
       <br><br><br> <br>
TOTO SUHARTO <br>
NIP. 196604181986021002
                    </td>
                  </table>
</center>
</body>
</html>
