<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="manifest" href="{{  asset('fav/manifest.json') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('fav/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">

<link rel="icon" type="image/png" href="{{ asset('logo.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" ></script> --}}

<style media="screen">
  .btn{
    cursor:pointer;
  }
</style>


   <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}" >
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
   <!-- CSS Files -->
   <link href="{{ asset('kit/css/bootstrap.min.css') }}" rel="stylesheet" />
   <link href="{{ asset('kit/css/light-bootstrap-dashboard.css?v=2.0.1') }}" rel="stylesheet" />


   <!--   Core JS Files   -->
   <script src="{{ asset('kit/js/core/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('kit/js/core/popper.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('kit/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
   <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
   <script src="{{ asset('kit/js/plugins/bootstrap-switch.js') }}"></script>


   <!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
   <script src="{{ asset('kit/js/light-bootstrap-dashboard.js?v=2.0.1') }}" type="text/javascript"></script>

</head>
<style media="screen">
  .navbar-brand{
    color:#ffff !important;
  }
  .nav-link{
      color:#ffff !important;
    }
    .nav-link:hover{
        color:yellow !important;
      }
</style>
<body>
    <div class="wrapper">
      @include('layouts.sidebar')
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " style="background-color: #980000" color-on-scroll="500">
                <div class=" container-fluid  ">

                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">


                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{  route('akun.index')}}">
                                    <span class="no-icon">Account</span>
                                </a>
                            </li>


                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                               <i class="fa fa-sign-out no-icon"  style="margin-right: 30px;">Logout</i>


                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">

                  @yield('content')
            {{-- <footer class="footer">
                <div class="container">
                    <nav>
                        <ul class="footer-menu">
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                        </p>
                    </nav>
                </div>
            </footer> --}}
        </div>
    </div>
</div>
</div>
</body>
</html>
@yield('script')
