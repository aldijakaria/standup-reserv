<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ReservationController@listAcara')->name('acara');

Route::group(['prefix'=>'admin'],function(){
  Route::get('/login', 'AdminController@showLoginForm')->name('admin.loginForm');
  Route::get('/login', 'AdminController@attemptLogin')->name('admin.login');

Route::group(['middleware'=>'auth:admin'],function(){
  Route::resource('movie', 'MovieController');
  Route::resource('user', 'UserController');
  Route::get('/home', 'HomeController@indexAdmin')->name('home.admin');
});
});
Route::group(['middleware'=>'auth'],function(){
  Route::get('/reservations', 'ReservationController@all')->('reserve.all');
  Route::get('/reservations/{id}', 'ReservationController@index');
  Route::post('/reservation/create', 'ReservationController@create');
  Route::post('/reservation/step2', 'ReservationController@addReservation');


  Route::get('/home', 'HomeController@index')->name('home.user');
});
  Auth::routes();
