@extends('layouts.app')

@section('content')

        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">User</div>
                    <div class="card-body">
                        <a href="{{ route('user.create') }}" class="btn btn-success btn-sm" title="Tambah User Baru">
                            <i class="fa fa-plus" aria-hidden="true"></i> Tambah Baru
                        </a>

                        @if ($alert=Session::get('alert'))
                        <div class="alert alert-info">
                          {{ $alert }}
                        </div>
                        @endif
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table" id="tabel">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Email</th><th>Password</th><th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($user as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td><td>{{ $item->email }}</td><td>{{ $item->password }}</td>
                                        <td>
                                            <a href="{{ route('user.show', $item->id) }}" title="View User"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</button></a>
                                            <a href="{{ route('user.edit', $item->id) }}" title="Edit User"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ route('user.destroy', $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus User" onclick="return confirm(&quot;Konfirmasi hapus?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script>
        $(document).ready( function () {
            $('#tabel').DataTable({
                "ordering": false
            });
        } );
        </script>
@endsection
